import { getID } from '../lib/ids';
import * as types from '../consts/action-types';

const initialState = [];

const reducer = (recipes = initialState, action) => {
  switch (action.type) {

    case types.ADD_RECIPE:
      return recipes.concat({
        id: getID(),
        title: action.title,
        favorite: false
      });

    case types.TOGGLE_FAVORITE:
      return recipes.map(
                recipe => recipe.id !== action.id
                  ? recipe
                  : { ...recipe, favorite: !recipe.favorite });

    default:
      return recipes;
  }
};

export default reducer;