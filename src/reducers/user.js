const initialState = {
  name: null
};

const userReducer = (user = initialState, action) => {
  switch (action.type) {
    case 'SET_NAME':
      return Object.assign({}, user, {
        name: action.name
      });

    default:
      return user
  }
};

export default userReducer