import reducer from './reducers/root';
import { getID } from './lib/ids';
import { createStore, applyMiddleware } from 'redux';
import logMiddleware from './middleware/log';

const initialState = {
  recipes: [
    { id: getID(), title: 'Waffles', favorite: true },
    { id: getID(), title: 'Omelette', favorite: false },
    { id: getID(), title: 'Shakshuka', favorite: true }
  ],
  user: {
    name: 'Boris'
  }
};

export default createStore(
  reducer,
  initialState,
  applyMiddleware(logMiddleware));