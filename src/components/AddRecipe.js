import React from 'react';
import { connect } from 'react-redux';
import { addRecipe } from '../actions/recipes';

class AddRecipe extends React.Component {
  constructor() {
    super();

    this.addRecipe = this.addRecipe.bind(this);
  }

  addRecipe(e) {
    e.preventDefault();

    this.props.addRecipe(this.title.value);

    this.title.value = '';
  }

  render() {
    return (
      <form onSubmit={ this.addRecipe }>
        <h2>Add Recipe</h2>
        <input type="text" ref={ elem => this.title = elem }/>
        <button>Add</button>
      </form>
    );
  }
}

AddRecipe.propTypes = {
  addRecipe: React.PropTypes.func.isRequired
};

// Connect

export default connect(null, { addRecipe })(AddRecipe);