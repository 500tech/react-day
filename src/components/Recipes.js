import React from 'react';
import { connect } from 'react-redux';
import Recipe from './Recipe';
import { toggleFavorite } from '../actions/recipes';

const Recipes = ({ recipes, toggleFavorite }) => (
  <ul className="recipes">
    { recipes.map(recipe =>
        <Recipe key={ recipe.id }
                recipe={ recipe }
                toggleFavorite={ toggleFavorite } />)
    }
  </ul>
);

Recipes.propTypes = {
  recipes: React.PropTypes.array.isRequired,
  toggleFavorite: React.PropTypes.func.isRequired
};

// Connection Code

const mapStateToProps = (state) => ({
  recipes: state.recipes
});

export default connect(mapStateToProps, { toggleFavorite })(Recipes);